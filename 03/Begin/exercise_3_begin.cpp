/*
//@HEADER
// ************************************************************************
//
//                        Kokkos v. 2.0
//              Copyright (2014) Sandia Corporation
//
// Under the terms of Contract DE-AC04-94AL85000 with Sandia Corporation,
// the U.S. Government retains certain rights in this software.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
// 1. Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution.
//
// 3. Neither the name of the Corporation nor the names of the
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY SANDIA CORPORATION "AS IS" AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL SANDIA CORPORATION OR THE
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
// LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
// NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Questions Contact  H. Carter Edwards (hcedwar@sandia.gov)
//
// ************************************************************************
//@HEADER
*/

// EXERCISE 3 Goal:
//   Define host mirrors and copy from the device to the host to check our solution

#include <limits>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <sys/time.h>

#include "idefix.hpp"

void ReadCommandLine( int &S, int &nrepeat,int argc, char* argv[]);


int main( int argc, char* argv[] )
{
  // default size is 2^22 .
  int S = pow( 2, 22 );
  int nrepeat = 100;  // number of repeats of the test

  // Check if user asked for a different problem size
  ReadCommandLine(S,nrepeat,argc,argv);
  

  Kokkos::initialize( argc, argv );
  
  {

  auto x = IdefixArray1D<double>("x",S);
  auto y = IdefixArray1D<double>("y",S);
  auto r = IdefixArray1D<double>("r",S);

  auto solution = static_cast<double*>(std::malloc(S * sizeof(double)));

  double a = 0.3;

  // Initialize x vector.
  idefix_for("init x",0,S, KOKKOS_LAMBDA(int i) {
    x( i ) = 1-3*i;
  });

  // Initialize y vector.
  idefix_for("init y",0,S, KOKKOS_LAMBDA(int i) {
    y( i ) = 1+i;
  });

  // Initialize solution (do not use idefix_for here!)
  for ( int i = 0; i < S; ++i ) {
    solution[ i ] = (a+1)+(1-3*a)*i;
  }

  // Timer products.
  //Kokkos::Timer timer;
  struct timeval begin, end;

  gettimeofday( &begin, NULL );

  for ( int repeat = 0; repeat < nrepeat; repeat++ ) {
    // Application: a*x+y

    idefix_for("main loop",0,S, KOKKOS_LAMBDA(int i) {
      r(i) = a*x(i) + y(i);
    });


    // Check result
    double error = 0;
    if ( repeat == ( nrepeat - 1 ) ) {
      // EXERCISE: use IdefixHostArray1D to define a host array identical to r

      // EXERCISE: use Kokkos::deep_copy(dst,src) to copy data from the device array to the host

      // EXERCISE: compare the solution to the *host copy* of r
      for ( int i = 0; i < S; ++i ) {
        error = error + std::fabs(r(i)-solution[i]);
      }
      error=error/S;
      printf( "  Error: %lf\n", error );
    }
  }

  gettimeofday( &end, NULL );

  // Calculate time.
  //double time = timer.seconds();
  double time = 1.0 * ( end.tv_sec - begin.tv_sec ) +
                1.0e-6 * ( end.tv_usec - begin.tv_usec );

  // Calculate bandwidth.
  // The x vector (of length S) is read once.
  // The y vector (of length S) is read once.
  // The solution (of length S) is written once.
  // double Gbytes = 1.0e-9 * double( sizeof(double) * ( 3 * S ) );
  double Gbytes = 1.0e-9 * double( sizeof(double) * ( 3*S ) );

  // Print results (problem size, time and bandwidth in GB/s).
  printf( "  S( %d ) nrepeat ( %d ) problem( %g MB ) time( %g s ) bandwidth( %g GB/s )\n",
          S, nrepeat, Gbytes * 1000, time, Gbytes * nrepeat / time );

  std::free(solution);

  }
  Kokkos::finalize();

  return 0;
}

void ReadCommandLine( int &S, int &nrepeat,int argc, char* argv[]) {
  // Read command line arguments.
  for ( int i = 0; i < argc; i++ ) {
    if ( ( strcmp( argv[ i ], "-S" ) == 0 ) || ( strcmp( argv[ i ], "-Size" ) == 0 ) ) {
      S = pow( 2, atof( argv[ ++i ] ) );
      printf( "  User S is %d\n", S );
    }
    else if ( strcmp( argv[ i ], "-nrepeat" ) == 0 ) {
      nrepeat = atoi( argv[ ++i ] );
    }
    else if ( ( strcmp( argv[ i ], "-h" ) == 0 ) || ( strcmp( argv[ i ], "-help" ) == 0 ) ) {
      printf( "  A*x+y Options:\n" );
      printf( "  -Size (-S) <int>:      exponent num, determines total matrix size 2^num (default: 2^22 = 4096*1024 )\n" );
      printf( "  -nrepeat <int>:        number of repetitions (default: 100)\n" );
      printf( "  -help (-h):            print this message\n\n" );
      exit( 1 );
    }
  }
}
